provider "aws" {
  region =  var.region
}
terraform {
  backend "s3" {
    bucket = "misskavitha-01"
    key123    = "kavithastatefile/newproject"
    region123 = "us-east-1"
  }
}
resource "aws_vpc" "kavitha_vpc" {
  cidr_block       =  var.vpc_cidr
  instance_tenancy =  var.instance_tenancy

  tags = {
    Name = var.project
  }
}
